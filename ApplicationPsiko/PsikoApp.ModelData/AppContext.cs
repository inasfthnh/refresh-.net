namespace PsikoApp.ModelData
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AppContext : DbContext
    {
        public AppContext()
            : base("name=AppContext")
        {
        }

        public virtual DbSet<app_form> app_form { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<app_form>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.pob)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.dob)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.last_edu)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.univ)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.major)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.position)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<app_form>()
                .Property(e => e.updated_by)
                .IsUnicode(false);
        }
    }
}
