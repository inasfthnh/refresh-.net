﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace PsikoApp.ModelView
{
    public class app_formView
    {
        public long id_form { get; set; }

        [Required(ErrorMessage = "Please enter your firstname")]
        [StringLength(250), Display(Name = "Name")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please enter the number only, min 10 number And max 13 number")]
        [StringLength(13), Display(Name = "Mobile Phone Number")]
        public string phone { get; set; }

        [StringLength(13), Display(Name = "Alternative Phone Number")]
        public string phone_alt { get; set; }

        [Required(ErrorMessage = "Please enter a valid email address")]
        [StringLength(100), Display(Name = "Email")]
        public string email { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [StringLength(100), Display(Name = "Place of Birth")]
        public string pob { get; set; }

        [Required]
        [StringLength(30), Display(Name = "Date Of Birth")]
        public string dob { get; set; }
        public string dob_y { get; set; }
        public string dob_m { get; set; }
        public string dob_d { get; set; }

        [Required(ErrorMessage = "please enter your last Education")]
        [StringLength(20), Display(Name = "Last Education")]
        public string last_edu { get; set; }

        [Required(ErrorMessage = "please enter your college")]
        [StringLength(100), Display(Name = "College / University")]
        public string univ { get; set; }

        [Required(ErrorMessage = "please enter your Major")]
        [StringLength(100), Display(Name = "Major")]
        public string major { get; set; }

        [Required(ErrorMessage = "please enter a position apply")]
        [StringLength(100), Display(Name = "Position Apply")]
        public string position { get; set; }

        [Required(ErrorMessage = "please select source")]
        [StringLength(100), Display(Name = "Source")]
        public string source { get; set; }

    }
}
