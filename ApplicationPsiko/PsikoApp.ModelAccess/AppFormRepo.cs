﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PsikoApp.ModelData;
using PsikoApp.ModelView;

namespace PsikoApp.ModelAccess
{
    public class AppFormRepo
    {
        //List
        public static List<app_formView> All()
        {
            List<app_formView> result = new List<app_formView>();
            using (var db = new AppContext())
            {
                result = (from p in db.app_form
                          select new app_formView
                          {
                              id_form = p.id_form,
                              name = p.name,
                              phone = p.phone,
                              phone_alt = p.phone_alt,
                              email = p.email,
                              pob = p.pob,
                              dob = p.dob,
                              last_edu = p.last_edu,
                              univ = p.univ,
                              major = p.major,
                              position = p.position,
                              source = p.source
                          }).ToList();
            }
            return result;
        }

        public static ResponseResult Create(app_formView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new AppContext())
                {
                        app_form form = new app_form();
                    // Create
                        entity.dob = entity.dob_y + "-" + entity.dob_m + "-" + entity.dob_d;

                        form.created_by = "admin";
                        form.created_date = DateTime.Now;
                        form.name = entity.name;
                        form.phone = entity.phone;
                        form.phone_alt = entity.phone_alt;
                        form.email = entity.email;
                        form.pob = entity.pob;
                        form.dob = entity.dob;
                        form.last_edu = entity.last_edu;
                        form.univ = entity.univ;
                        form.major = entity.major;
                        form.position = entity.position;
                        form.source = entity.source;

                        db.app_form.Add(form);
                        db.SaveChanges();

                        result.Message = "Form berhasil disubmit";
                        result.Entity = entity;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(app_formView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new AppContext())
                {
                        app_form form = db.app_form
                            .Where(o => o.id_form == entity.id_form)
                            .FirstOrDefault();
                        if (form != null)
                        {
                            entity.dob = entity.dob_y + "-" + entity.dob_m + "-" + entity.dob_d;

                            form.name = entity.name;
                            form.phone = entity.phone;
                            form.phone_alt = entity.phone_alt;
                            form.email = entity.email;
                            form.pob = entity.pob;
                            form.dob = entity.dob;
                            form.last_edu = entity.last_edu;
                            form.univ = entity.univ;
                            form.major = entity.major;
                            form.position = entity.position;
                            form.source = entity.source;
                            form.updated_by = "admin";
                            form.updated_date = DateTime.Now;
                            db.SaveChanges();

                            result.Message = "Form berhasil diedit";
                            result.Entity = entity;
                        } else {                        
                            result.Success = false;
                        }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static app_formView ById(long id_form)
        {
            app_formView result = new app_formView();
            try
            {
                using (var db = new AppContext())
                {
                    result = (from p in db.app_form
                              where (p.id_form == id_form)
                              select new app_formView
                              {
                                  id_form = p.id_form,
                                  name = p.name,
                                  phone = p.phone,
                                  phone_alt = p.phone_alt,
                                  email = p.email,
                                  pob = p.pob,
                                  dob = p.dob,
                                  last_edu = p.last_edu,
                                  univ = p.univ,
                                  major = p.major,
                                  position = p.position,
                                  source = p.source
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new app_formView() : result;
        }

        public static ResponseResult Delete(long id_form)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new AppContext())
                {
                    app_form form = db.app_form
                        .Where(o => o.id_form == id_form)
                        .FirstOrDefault();

                    app_form oldForm = form;
                    result.Entity = oldForm;

                    if (form != null)
                    {
                        db.app_form.Remove(form);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Form tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
