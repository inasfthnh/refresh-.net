﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PsikoApp.ModelData;
using PsikoApp.ModelAccess;
using PsikoApp.ModelView;

namespace ApplicationPsiko.Web.Controllers
{
    public class AppFormController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexList()
        {
            return View();
        }

        public ActionResult List()
        {
            List<app_formView> list = AppFormRepo.All();
            return PartialView("_List", list);
        }

        //Get
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(app_formView model)
        {
            ResponseResult result = AppFormRepo.Create(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Edit(long id_form)
        {
            return PartialView("_Edit", AppFormRepo.ById(id_form));
        }

        [HttpPost]
        public ActionResult Edit(app_formView model)
        {
            ResponseResult result = AppFormRepo.Edit(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Delete(long id_form)
        {
            return PartialView("_Delete", AppFormRepo.ById(id_form));
        }

        [HttpPost]
        public ActionResult Delete(app_formView model)
        {
            ResponseResult result = AppFormRepo.Delete(model.id_form);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }
    }
}